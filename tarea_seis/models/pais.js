'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var PaisSchema =  new Schema(
    {
        nombre: {
            type: String,
            trim:true,
            default:'',
            required:'Insertar una nombre por favor',
            index:{
                unique: true,
                dropDups: true
            }
        }
    },
    { 
        timestamps :true
    } 
);

var Pais = mongoose.model('Pais', PaisSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports = Pais;