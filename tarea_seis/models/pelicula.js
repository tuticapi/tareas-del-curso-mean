'use strict';

var database = require('../database'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var PeliculaSchema = new Schema(
    {
        numero: {
            type: String,
            trim: true,
            required: 'Insertar una numero por favor',
            index: {
                unique: false,
                dropDups: true
            }
        },
        mnombre: {
            type: String,
            trim: true,
            default: '',
            required: 'Insertar una nombre de pelicula por favor',
            index: {
                unique: true,
                dropDups: true
            }
        },
        duracion: {
            type: String,
            trim: true,
            required: 'Insertar una duracion por favor',
            index: {
                unique: false,
                dropDups: true
            }
        },
        Pais: {
            type: Schema.ObjectId,
            ref: 'Director',
            required: 'Insertar un id deDirector por favor'
        }
    },
    {
        timestamps: true
    }
);

var Pelicula = mongoose.model('Pelicula', PeliculaSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports = Pelicula;