'use strict';

var database = require('../database'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var DirectorSchema = new Schema(
    {
        nombre: {
            type: String,
            trim: true,
            default: '',
            required: 'Insertar una nombre por favor',
            index: {
                unique: false,
                dropDups: true
            }
        },
        apellidoP: {
            type: String,
            trim: true,
            default: '',
            required: 'Insertar una apellidopaterno por favor',
            index: {
                unique: false,
                dropDups: true
            },
        },
        apellidoM: {
            type: String,
            trim: true,
            default: '',
            required: 'Insertar una apellido materno por favor',
            index: {
                unique: false,
                dropDups: true
            },
        },
        Pais: {
            type: Schema.ObjectId,
            ref:'Pais',
            required: 'Insertar un id de pais por favor'
        }


    },
    {
        timestamps: true
    }
);

var Director = mongoose.model('Director', DirectorSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports = Director;