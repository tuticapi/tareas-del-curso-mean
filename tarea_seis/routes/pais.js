'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var paisController = require('../controllers/pais')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.post('/pais',paisController.postPais)
api.get('/pais/:id?',paisController.getPais)
api.get('/paises/',paisController.getPaises)
api.put('/pais/:id?',paisController.updatePais)

//Para utilizarlo enotrs ficheros e importar
module.exports = api;