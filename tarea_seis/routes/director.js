'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var directorController = require('../controllers/director')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.post('/director',directorController.initPost,
                directorController.midPost,
                directorController.postDirector),

 api.get('/directores/:paisId?',directorController.initGetDirectores,
                directorController.getDirectores);               


//Para utilizarlo enotrs ficheros e importar
module.exports = api;