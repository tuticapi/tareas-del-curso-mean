'use strict'
//definimmos quenuestro esquema se podra llamar auto
//enlas operaciones de nuestro controlador
var mongoose = require('mongoose')
var Pais = require('../models/pais');
var Director = require('../models/director')
var Pelicula = require('../models/pelicula')


function postPais(req, res) {
    if (Object.keys(req.body).length === 0) {
        res.status(409).send({ message: 'Por favor envia un cuerpo' })

    } else {
        //var autoId = req.params.id;
        //definimos el objeto que seguardara como documento
        var newPais = new Pais(req.body);
        newPais.save(function (err, PaisSaved) {
            if (err) {
                console.log(err)
                res.status(500).send({ message: "Error al guardar Pais", error: err })
            } else {
                res.status(200).send({ saved: PaisSaved })
            }

        })
    }
}

function getPais(req, res){
    //obtenemoselid que llega comoparametro
    var paisId = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(paisId);
    console.log("entra")
    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Pais.findById(paisId,function(err, pais){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al obtener el pais." , error:err})
            }else{
                if(!pais){
                    res.status(404).send({message:"Noexiste el pais con el id proporcionado"})
                }else{
                    res.status(200).send({pais})
                }
            }
        })
    }
  
}

function getPaises(req, res){
    Pais.find({}).sort('nombre').exec(function(err, paisesFounded){
        if(err){
            console.log(err)
                res.status(200).send({message: "Error al optener paises ", error :err})
        }else{
                res.status(200).send({paises:paisesFounded})
        }

    })
}


function updatePais(req, res){
/*
    if(Object.keys(req.body).lenght === 0){

         res.status(409).send({message: "Por favor envia un cuerpo. "});

    }else{
        //if(!mongoose.Types)
        if(req.params.pais.Id){
             res.status(409).send({message: "Por favor envia un id de pais. "});
        }else{
            if(!mongoose.Types.ObejctId.isValid(req.params.paisId)){

            }
        }
    }
    */
    //obtenemoselid que llega comoparametro
    var paisId = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(paisId);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Pais.findByIdAndUpdate(paisId,req.body,function(err, paisUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al actualizar el pais." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
              Pais.findById(paisId,function(err, paisNuevo){
               
                res.status(200).send({viejo:paisUpdate,nuevo:paisNuevo})
              })
            }
        })
    }
}

module.exports = {
    postPais,
    getPais,
    getPaises,
    updatePais
}