var bodyParser = require('body-parser')
var express = require('express')


var app = express()
var pais_routes = require('./routes/pais'),
director_routes = require('./routes/director')

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())




app.use('/api',pais_routes),
app.use('/api',director_routes);

module.exports = app;