import {Component,OnInit} from '@angular/core';
import {Router, ActivatedRoute,Params} from '@angular/router';
import {AutoService} from '../services/auto.service';
import {Auto} from '../models/auto';


@Component({
    selector: 'autos-modifica',
    templateUrl:'app/views/auto-agrega.html',
    providers:[AutoService]
})


export class AutoModificaComponent implements OnInit{


    public errorMensage:any;
    public auto:Auto;
    public title :string;

    constructor(
        private _autoService:AutoService,
        private _route:ActivatedRoute,
        private _router:Router
    ){
        this.title = "Modificar un registro"
    }
        ngOnInit(){
            console.log("oninit")
        this.auto = new Auto("","",null,"");
        this.getAuto();
    }

    getAuto(){
        this._route.params.forEach((parms:Params)=>{
            let _id = parms['id'];
            this._autoService.getauto(_id).subscribe(
                response =>{
                    this.auto = response.auto;
                    if(!this.auto){
                        this._router.navigate(['/']);
                    }
                },
                error=>{
                    this.errorMensage = <any>error;
                    if(this.errorMensage!=null){
                        alert("error en la petición")
                    }
                }
            )
          })
    }


    public onSubmit(){
  console.log("entra")
        this._route.params.forEach((params:Params)=>{
            let _id = params['id'];
            this._autoService.putAuto(_id,this.auto).subscribe(
             response =>{
                if(!response.nuevo){
                   this._router.navigate(['/']);
                }else{
                    this.auto = response.nuevo;
                    this._router.navigate(['/auto',response.nuevo._id])
                }
            },
               error => {
                this.errorMensage=<any>error;
                if(this.errorMensage!=null){
                    console.log('Error en la peticion');
                     alert('Error en la peticion');
                }
            }
            )

        })    
}

}


         
        