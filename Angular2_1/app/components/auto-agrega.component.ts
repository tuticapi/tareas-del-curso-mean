import {Component,OnInit} from '@angular/core';
import {Router, ActivatedRoute,Params} from '@angular/router';
import {AutoService} from '../services/auto.service';
import {Auto} from '../models/auto';


@Component({
    selector: 'auto-agrega',
    templateUrl:'app/views/auto-agrega.html',
    providers:[AutoService]
})

export class AutoAgregaComponent implements OnInit{

    public errorMensage:string;
    public auto:Auto;
    public title :string;

    constructor(
        private _autoService:AutoService,
        private _route:ActivatedRoute,
        private _router:Router
    ){
        this.title = "Crear un registro"
    }

        ngOnInit(){
        this.auto = new Auto("","",null,"");
    }


    public onSubmit(){
        console.log(this.auto)
        this._autoService.postAuto(this.auto).subscribe(
            response =>{
                if(!response.saved){
                    alert("Error en el servidor")
                }else{
                    this.auto = response.saved;
                    this._router.navigate(['/auto',response.saved._id])
                }
            },
            error => {
                this.errorMensage=<any>error;
                if(this.errorMensage!=null){
                    console.log('Error en la peticion');
                }
            }
        )
    }

}
