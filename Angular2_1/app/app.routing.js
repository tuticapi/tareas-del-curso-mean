"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = require("@angular/router");
const autos_component_1 = require("./components/autos.component");
const auto_ver_component_1 = require("./components/auto-ver.component");
const auto_agrega_component_1 = require("./components/auto-agrega.component");
const autos_modifica_components_1 = require("./components/autos-modifica.components");
const appRoutes = [
    { path: '', component: autos_component_1.AutosComponent },
    { path: 'auto/:id', component: auto_ver_component_1.AutosVerComponent },
    { path: 'crear-auto', component: auto_agrega_component_1.AutoAgregaComponent },
    { path: 'modificar-auto/:id', component: autos_modifica_components_1.AutoModificaComponent },
    { path: '**', component: autos_component_1.AutosComponent }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map