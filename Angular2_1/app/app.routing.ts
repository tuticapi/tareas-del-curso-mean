import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AutosComponent } from './components/autos.component';

import {AutosVerComponent} from './components/auto-ver.component';

import {AutoAgregaComponent} from './components/auto-agrega.component';

import {AutoModificaComponent} from './components/autos-modifica.components';

const appRoutes:Routes = [

    {path:'', component:AutosComponent},
    {path:'auto/:id', component:AutosVerComponent},
    {path:'crear-auto', component:AutoAgregaComponent},
    {path:'modificar-auto/:id', component:AutoModificaComponent},
    {path:'**', component:AutosComponent}
];

export const appRoutingProviders: any[] = [];
export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);