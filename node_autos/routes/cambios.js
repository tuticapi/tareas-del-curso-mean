'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var cambiosController = require('../controllers/cambios')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.get('/cambios/',cambiosController.getCambios)
api.post('/cambios',cambiosController.saveCambios)
api.put('/cambios/:id?',cambiosController.updateCambios)
api.delete('/cambios/:id?',cambiosController.deleteCambios)
//Para utilizarlo enotrs ficheros e importar
module.exports = api;