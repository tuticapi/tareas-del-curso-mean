'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var CambiosSchema =  new Schema(
    {
        Moneda: {
            type: String,
            trim:true,
            default:'',
            required:'Insertar una moneda por favor',
            index:{
                unique: true,
                dropDups: true
            }
        },

        ValorDolar:{
            type: Number,
            default:'',
            required:'Insertar un valor en dolares por favor',
            index:{
                unique: true,
                dropDups: true
            }
        },

        Signo:{
            type: "String",
            default:'$',
            required:'Insertar un signoporfavor',
            index:{
                unique: false,
                dropDups: true
            }
        }
    },
    { 
        timestamps :true
    } 
);

var Cambios = mongoose.model('Cambios', CambiosSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports =Cambios;