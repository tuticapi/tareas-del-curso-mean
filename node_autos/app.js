var bodyParser = require('body-parser')
var express = require('express')


var app = express()
var api = require('./routes/auto'),
cambios = require('./routes/cambios')

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.use(function(req,res,next){
 res.setHeader('Access-Control-Allow-Origin', '*');
     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
})


app.use('/api',api),
app.use('/api',cambios);


module.exports = app;