'use strict'
//definimmos quenuestro esquema se podra llamar auto
//enlas operaciones de nuestro controlador
var mongoose = require('mongoose')
var Cambios = require('../models/cambios')





function getCambios(req, res){
    Cambios.find({}).sort('anio').exec(function(err, cambio){
        if(err){
            console.log(err)
                res.status(200).send({message: "Error al optener autos", error :err})
        }else{
                res.status(200).send({cambio})
        }

    })
}

function saveCambios(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //definimos el objeto que seguardara como documento
    var objCambio =new Cambios(req.body);
    objCambio.save(function(err, cambioSaved){
        if(err){
            console.log(err)
                res.status(500).send({message: "Error al guardar auto", error :err})
        }else{
                res.status(200).send({saved:cambioSaved})
        }

    })
}

function updateCambios(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Cambios.findByIdAndUpdate(id,req.body,function(err, cambioUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al actualizar el auto." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
              Cambios.findById(id,function(err, cambioNuevo){
               
                res.status(200).send({viejo:cambioUpdate,nuevo:cambioNuevo})
              })
            }
        })
    }
}

function deleteCambios(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Cambios.findByIdAndUpdate(id,req.body,function(err, autoUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al obtener el auto." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
                Cambios.remove(function(err){
                    if(err){
                        console.log(err)
                            res.status(500).send({message: "Error al eliminar  cambio", error :err})
                    }else{
                            res.status(200).send({message:"El cambio ha eliminado"})
                    }

                })
            }
        })
    }
}

//definimos los métodos que puedenser alcanzables
module.exports = {
    getCambios,
    saveCambios,
    updateCambios,
    deleteCambios
}