'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var idiomasController = require('../controllers/idiomas')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.get('/idiomas/',idiomasController.getLanguages)
api.post('/idiomas',idiomasController.saveLanguages)
api.put('/idiomas/:id?',idiomasController.updateLanguages)
api.delete('/idiomas/:id?',idiomasController.deleteLanguages)


//Para utilizarlo enotrs ficheros e importar
module.exports = api;