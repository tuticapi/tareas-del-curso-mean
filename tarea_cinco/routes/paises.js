'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var paisesController = require('../controllers/paises')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.get('/paises/',paisesController.getPaises)
api.post('/paises',paisesController.savePaises)
api.put('/paises/:id?',paisesController.updatePaises)
api.delete('/paises/:id?',paisesController.deletePaises)
//Para utilizarlo enotrs ficheros e importar
module.exports = api;