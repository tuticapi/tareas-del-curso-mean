'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var ciudadesController = require('../controllers/ciudad')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.get('/ciudades/',ciudadesController.getCities)
api.post('/ciudades',ciudadesController.saveCities)
api.put('/ciudades/:id?',ciudadesController.updateCities)
api.delete('/ciudades/:id?',ciudadesController.deleteCities)
//Para utilizarlo enotrs ficheros e importar
module.exports = api;