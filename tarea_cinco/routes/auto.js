'use strict'

//importamosexpress

var express = require('express')


//importamoselcontrolador
var autoController = require('../controllers/auto')


//Instanciamos un objeto Router
var api = express.Router();

//Definimoselñrecurso GET con url : /api/auto/:id , recibe
//un parametro y se procesa enelmetodo prueba del controlador
//autoController

//api.get('/auto/:id?',autoController.prueba)
api.get('/auto/:id?',autoController.getAuto)
api.get('/autos/',autoController.getAutos)
api.post('/auto',autoController.saveAuto)
api.put('/auto/:id?',autoController.updateAuto)
api.delete('/auto/:id?',autoController.deleteAuto)
//Para utilizarlo enotrs ficheros e importar
module.exports = api;