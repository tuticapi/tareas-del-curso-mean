var bodyParser = require('body-parser')
var express = require('express')


var app = express()
var api = require('./routes/auto'),
cambios = require('./routes/cambios'),
paises = require('./routes/paises'),
ciudades = require('./routes/ciudades'),
idiomas = require('./routes/idiomas');

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())


app.use('/api',api),
app.use('/api',cambios),
app.use('/api',paises),
app.use('/api',ciudades),
app.use('/api',idiomas);

module.exports = app;