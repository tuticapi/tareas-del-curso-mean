'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var CitiesSchema =  new Schema(
    {
        Nombre: {
            type: String,
            trim:true,
            default:'',
            required:'Insertar una ciudad por favor',
            index:{
                unique: true,
                dropDups: true
            }
        },

        NumeroHabitantes:{
            type: Number,
            default:'',
            required:'Insertar un numero de habitantes por favor',
            index:{
                unique: false,
                dropDups: true
            }
        },

        ladaTelefonica:{
            type: Number,
            default:'',
            required:'Insertar una lada',
            index:{
                unique: false,
                dropDups: true
            }
        }
    },
    { 
        timestamps :true
    } 
);

var Cities = mongoose.model('cities', CitiesSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports = Cities;