'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var PaisesSchema =  new Schema(
    {
        Nombre: {
            type: String,
            trim:true,
            default:'',
            required:'Insertar una pais por favor',
            index:{
                unique: true,
                dropDups: true
            }
        },

        Continente:{
            type: String,
            default:'',
            required:'Insertar un continente de pais por favor',
            index:{
                unique: false,
                dropDups: true
            }
        },

        Idioma:{
            type: "String",
            default:'$',
            required:'Insertar un idioma',
            index:{
                unique: false,
                dropDups: true
            }
        }
    },
    { 
        timestamps :true
    } 
);

var Pais = mongoose.model('countries', PaisesSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports = Pais;