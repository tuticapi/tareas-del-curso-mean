'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var AutoSchema =  new Schema(
    {
        marca: {
            type: String,
            trim:true,
            default:'',
            required:'Insertar una marca por favor',
            index:{
                unique: true,
                dropDups: true
            }
        },

        modelo:{
            type: String,
            default:'',
            required:'Insertar un modelo por favor',
            index:{
                unique: false,
                dropDups: true
            }
        },

        anio:{
            type: Number,
            default:'1930',
            required:'Insertar un año por favor',
            index:{
                unique: false,
                dropDups: true
            }
        },

        version:{
            type: String,
            default:'',
            required:'Insertar una version  favor',
            index:{
                unique: false,
                dropDups: true
            }        
        }
    },
    { 
        timestamps :true
    } 
);

var Auto = mongoose.model('Auto', AutoSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports =Auto;