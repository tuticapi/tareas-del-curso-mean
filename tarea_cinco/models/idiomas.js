'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

//representacion deltipo dedocumentos
var LanguagesSchema =  new Schema(
    {
        Nombre: {
            type: String,
            trim:true,
            default:'',
            required:'Insertar un lenguaje por favor',
            index:{
                unique: true,
                dropDups: true
            }
        },

        NumeroDeParlantes:{
            type: Number,
            default:'',
            required:'Insertar un numero de hablantes por favor',
            index:{
                unique: false,
                dropDups: true
            }
        }
    },
    { 
        timestamps :true
    } 
);

var languages = mongoose.model('languages', LanguagesSchema);
//podra ser accesido desde cualquier parte si se importa

module.exports = languages;