'use strict'
//definimmos quenuestro esquema se podra llamar pais
//enlas operaciones de nuestro controlador
var mongoose = require('mongoose')
var Paises = require('../models/pais')





function getPaises(req, res){
    Paises.find({}).sort('anio').exec(function(err, pais){
        if(err){
            console.log(err)
                res.status(200).send({message: "Error al optener pais", error :err})
        }else{
                res.status(200).send({pais})
        }

    })
}

function savePaises(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //definimos el objeto que seguardara como documento
    var objPais =new Paises(req.body);
    objPais.save(function(err, paisSaved){
        if(err){
            console.log(err)
                res.status(500).send({message: "Error al guardar pais", error :err})
        }else{
                res.status(200).send({saved:paisSaved})
        }

    })
}

function updatePaises(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID
    console.log(id)
    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Paises.findByIdAndUpdate(id,req.body,function(err, paisUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al actualizar el pais." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
              Paises.findById(id,function(err, paisNuevo){
               
                res.status(200).send({viejo:paisUpdate,nuevo:paisNuevo})
              })
            }
        })
    }
}

function deletePaises(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Paises.findByIdAndUpdate(id,req.body,function(err, paisUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al obtener el pais." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
                Paises.remove(function(err){
                    if(err){
                        console.log(err)
                            res.status(500).send({message: "Error al eliminar  pais", error :err})
                    }else{
                            res.status(200).send({message:"El pais ha eliminado"})
                    }

                })
            }
        })
    }
}

//definimos los métodos que puedenser alcanzables
module.exports = {
    getPaises,
    savePaises,
    updatePaises,
    deletePaises
}