'use strict'
//definimmos quenuestro esquema se podra llamar ciudad
//enlas operaciones de nuestro controlador
var mongoose = require('mongoose')
var Cities = require('../models/ciudad')





function getCities(req, res){
    Cities.find({}).exec(function(err, ciudad){
        if(err){
            console.log(err)
                res.status(200).send({message: "Error al optener ciudad", error :err})
        }else{
                res.status(200).send({ciudad})
        }

    })
}

function saveCities(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //definimos el objeto que seguardara como documento
    var objCity =new Cities(req.body);
    objCity.save(function(err, ciudadSaved){
        if(err){
            console.log(err)
                res.status(500).send({message: "Error al guardar ciudad", error :err})
        }else{
                res.status(200).send({saved:ciudadSaved})
        }

    })
}

function updateCities(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID
    console.log(id)
    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Cities.findByIdAndUpdate(id,req.body,function(err, ciudadUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al actualizar el ciudad." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
              Cities.findById(id,function(err, ciudadNuevo){
               
                res.status(200).send({viejo:ciudadUpdate,nuevo:ciudadNuevo})
              })
            }
        })
    }
}

function deleteCities(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Cities.findByIdAndUpdate(id,req.body,function(err, ciudadUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al obtener el ciudad." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
                Cities.remove(function(err){
                    if(err){
                        console.log(err)
                            res.status(500).send({message: "Error al eliminar  ciudad", error :err})
                    }else{
                            res.status(200).send({message:"El ciudad ha eliminado"})
                    }

                })
            }
        })
    }
}

//definimos los métodos que puedenser alcanzables
module.exports = {
    getCities,
    saveCities,
    updateCities,
    deleteCities
}