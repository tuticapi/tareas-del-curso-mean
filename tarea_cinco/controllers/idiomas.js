'use strict'
//definimmos quenuestro esquema se podra llamar language
//enlas operaciones de nuestro controlador
var mongoose = require('mongoose')
var Languages = require('../models/idiomas')





function getLanguages(req, res){
    Languages.find({}).exec(function(err, language){
        if(err){
            console.log(err)
                res.status(200).send({message: "Error al optener language", error :err})
        }else{
                res.status(200).send({language})
        }

    })
}

function saveLanguages(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //definimos el objeto que seguardara como documento
    var objlanguage =new Languages(req.body);
    objlanguage.save(function(err, languageSaved){
        if(err){
            console.log(err)
                res.status(500).send({message: "Error al guardar language", error :err})
        }else{
                res.status(200).send({saved:languageSaved})
        }

    })
}

function updateLanguages(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID
    console.log(id)
    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Languages.findByIdAndUpdate(id,req.body,function(err, languageUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al actualizar el language." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
              Languages.findById(id,function(err, languageNuevo){
               
                res.status(200).send({viejo:languageUpdate,nuevo:languageNuevo})
              })
            }
        })
    }
}

function deleteLanguages(req, res){
    //obtenemoselid que llega comoparametro
    var id = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(id);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Languages.findByIdAndUpdate(id,req.body,function(err, languageUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al obtener el language." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
                Languages.remove(function(err){
                    if(err){
                        console.log(err)
                            res.status(500).send({message: "Error al eliminar  language", error :err})
                    }else{
                            res.status(200).send({message:"El language ha eliminado"})
                    }

                })
            }
        })
    }
}

//definimos los métodos que puedenser alcanzables
module.exports = {
    getLanguages,
    saveLanguages,
    updateLanguages,
    deleteLanguages
}