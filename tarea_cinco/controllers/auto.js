'use strict'
//definimmos quenuestro esquema se podra llamar auto
//enlas operaciones de nuestro controlador
var mongoose = require('mongoose')
var Auto = require('../models/auto')


//definimos el metodo aserconsumido
//desde el archivo de rutas
function prueba (req,res){

    if(req.params.id){
        var id = req.params.id
    }else{
        var id = "SIN ID"
    }
    res.status(200).send(
        {
            message:"Este es el id "+id
        }
    )
}


function getAuto(req, res){
    //obtenemoselid que llega comoparametro
    var autoId = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(autoId);
    console.log("entra")
    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Auto.findById(autoId,function(err, auto){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error alobtener el auto." , error:err})
            }else{
                if(!auto){
                    res.status(404).send({message:"Noexiste elauto con el id proporcionado"})
                }else{
                    res.status(200).send({auto})
                }
            }
        })
    }
  
}

function getAutos(req, res){
    Auto.find({}).sort('anio').exec(function(err, autos){
        if(err){
            console.log(err)
                res.status(200).send({message: "Error al optener autos", error :err})
        }else{
                res.status(200).send({autos})
        }

    })
}

function saveAuto(req, res){
    //obtenemoselid que llega comoparametro
    var autoId = req.params.id;
    //definimos el objeto que seguardara como documento
    var auto =new Auto(req.body);
    auto.save(function(err, autoSaved){
        if(err){
            console.log(err)
                res.status(500).send({message: "Error al guardar auto", error :err})
        }else{
                res.status(200).send({saved:autoSaved})
        }

    })
}

function updateAuto(req, res){
    //obtenemoselid que llega comoparametro
    var autoId = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(autoId);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Auto.findByIdAndUpdate(autoId,req.body,function(err, autoUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al actualizar el auto." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
              Auto.findById(autoId,function(err, autoNuevo){
               
                res.status(200).send({viejo:autoUpdate,nuevo:autoNuevo})
              })
            }
        })
    }
}

function deleteAuto(req, res){
    //obtenemoselid que llega comoparametro
    var autoId = req.params.id;
    //verificamos sielparametro enviado es unobjetID

    var IdValido = mongoose.Types.ObjectId.isValid(autoId);

    if(!IdValido){
        //Si no es valido mostramos un mensaje de Id invalido
          res.status(409).send({message: "Id Invalido. "});
    }else{
        //Buscamos un doc por id
        Auto.findByIdAndUpdate(autoId,req.body,function(err, autoUpdate){
            if(err){
                console.log(err)
                res.status(500).send({message:"Error al obtener el auto." , error:err})
            }else{
                //Si se actualiza correctamente buscamos nuevamente en base,
                //ya que el callback nos retorna un objecto pero no es el actualizadosi no el viejo
                Auto.remove(function(err){
                    if(err){
                        console.log(err)
                            res.status(500).send({message: "Error al weliminar  auto", error :err})
                    }else{
                            res.status(200).send({message:"Elautose ha eliminado"})
                    }

                })
            }
        })
    }
}

//definimos los métodos que puedenser alcanzables
module.exports = {
    prueba,
    getAuto,
    getAutos,
    saveAuto,
    updateAuto,
    deleteAuto
}