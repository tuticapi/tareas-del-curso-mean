/**
 * Diego Magaldi
 */


'use strict'


var express = require('express')
var bodyParser = require('body-parser')
var app = express()


var port = process.env.PORT || 7070

app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json())

app.get('/api/:operador1?/:operador2?/:operando?', function(req, res){

var operador1;
var operador2;
var resultado =0.0;
var operando = "";
	
if(req.params.operador1 && req.params.operador2 && req.params.operando){
	operador1 = req.params.operador1
	operador2 = req.params.operador2
	operando = (req.params.operando)
	console.log(req.params.operando + " " +  req.params.operador1  +  "  " +  req.params.operador2)
	switch(operando){
        case "+":
        resultado = parseFloat(operador1) + parseFloat(operador2)
        break;

        case "-":
        resultado += parseFloat(operador1) -  parseFloat(operador2)
        break;

        case "x":
        resultado += parseFloat(operador1) * parseFloat(operador2)

        break  
        case "/":
        resultado += parseFloat(operador1) / parseFloat(operador2)
        break              
    }
}else{

	resultado = "Falto un parametro"
}

	res.status(200).send({
		arreglo: [{objeto_1: 1, objeto_2: 2}, {objeto_1:1, objeto_2:2}],
		ejemplo: "Esto es un ejemplode respuesta, resultado: " + resultado
	})
});

app.listen(7070,function(){

	console.log('Esto es un ejemplode una API puerto ' + port)
});

